<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>
<h2>Tentukan Nilai</h2>
<?php
function tentukan_nilai($number)
{
    //  kode disini
    if ($number >=85 && $number <=100) {
        echo "<br>Sangat Baik ";
    } elseif ($number >=70) {
        echo "<br>Baik ";
    }elseif ($number >=60) {
        echo "<br>Cukup ";
    }else{
        echo "<br>Kurang ";
    }
    return $number ;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
</body>
</html>